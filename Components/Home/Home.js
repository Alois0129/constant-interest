import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions } from 'react-native';
import MapView from 'react-native-maps';
import { Location } from 'expo-location';
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions';
import { Constants } from 'expo';

class Home extends React.Component {

    // header nav options
    static navigationOptions = {
        headerShown: false
    };

    camera = null;

    state = {
        mapRegion : { latitude: 37.78825, longitude: -122.4324, latitudeDelta: 0.0922, longitudeDelta: 0.0421 },
        locationResult: null,
        location: {coords: { latitude: 45.041451, longitude: 3.879739}},
        captures: {mark: {image: "", latitude: 45.041451, longitude: 3.879739} }, // stockage des photos ainsi que de la position actuelle de l'utilisateur
        hasCameraPermission: null,
        cameraType: Camera.Constants.Type.back,
        flashMode: Camera.Constants.FlashMode.on,
    };

    setCameraType = (cameraType) => this.setState({ cameraType });

    takePicture = async () => {
        const options = { quality: 0.5, base64: true };
        const data = await this.camera.takePictureAsync()
        .then(data => this.setState({captures: {mark: {image: data.uri, latitude: this.state.mapRegion.latitude, longitude: this.state.mapRegion.longitude} } }))
        .catch(error => console.log('eror', error));
    }

    _handleMapRegionChange = mapRegion => {
        this.setState({ mapRegion });
    };

    _getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                locationResult: 'Permission to access location was denied',
                location,
            });
        }
        let location = await Location.getCurrentPositionAsync({});
        this.setState({ locationResult: JSON.stringify(location), location });
    };

    async componentDidMount() {
        // Camera
        const camera = await Permissions.askAsync(Permissions.CAMERA);
        const audio = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
        const hasCameraPermission = (camera.status === 'granted' && audio.status === 'granted');
        this.setState({ hasCameraPermission });

        // Location
        this._getLocationAsync();
    };


    render() {
        // navigation
        const {navigate} = this.props.navigation;

        console.log(this.state.captures);

        const { hasCameraPermission, flashMode, cameraType, capturing, captures } = this.state;
        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>Access to camera has been denied.</Text>;
        }

        return (

            <View style={styles.container}>

                <MapView
                    style={styles.map}
                    region={{
                        latitude: this.state.location.coords.latitude,
                        longitude: this.state.location.coords.longitude,
                        latitudeDelta: 0.0022,
                        longitudeDelta: 0.0421
                    }}
                    onRegionChange={this._handleMapRegionChange}
                    coordinate={this.state.location.coords}
                    title="My Marker"
                    description="Some description"
                />

                <MapView.Marker
                    coordinate={{
                        latitude: this.state.captures.mark.latitude,
                        longitude: this.state.captures.mark.longitude,
                    }}
                    source={ this.state.captures.mark.image } // J'ai essayé de placer mon image en marker mais sans succès
                >
                    <Image style={styles.markerImage} source={{uri: this.state.captures.mark.image}}/>
                </MapView.Marker>

                <TouchableOpacity style={styles.cameraButton} onPress={this.takePicture.bind(this)}>
                    <Image style={styles.cameraIcon} source={require('../../Images/icons/photo.png')}/>
                </TouchableOpacity>

                <View style={styles.cameraView}>
                    <Camera
                        style={styles.camera}
                        type={cameraType}
                        ref={camera => this.camera = camera}
                    />
                </View>

            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },

    cameraButton: {
        width: 80,
        height: 80,
        borderColor: 'white',
        borderWidth: 2,
        backgroundColor: 'transparent',
        position: 'absolute',
        bottom: 30,
        left: '50%',
        transform: [
            { translateX: - 40 },
        ],
        borderRadius: 50,
        shadowColor: "#000",
        shadowOffset: {
            width: 5,
            height: 5,
        },
        shadowOpacity: 1,
        shadowRadius: 0,
        elevation: 1,
        opacity: 0.8,
    },
    cameraIcon: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: [
            { translateX: - 20 },
            { translateY: - 15 },
        ],
    },

    cameraView: {
        zIndex: 2,
        width: '30%',
        height: '30%',
        position: 'absolute',
        top: 0,
        right: 0,
    },
    camera: {
        flex: 1,
    },

    markerImage: {
        width: 100,
        height: 70,
    }
});

export default Home;
