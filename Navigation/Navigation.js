import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Home from '../Components/Home/Home';

const MainNavigator = createStackNavigator({
  Home: {screen: Home},
});

const Route = createAppContainer(MainNavigator);
export default Route;
